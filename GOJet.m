(* ::Package:: *)

BeginPackage["GOJet3`"]

eps::usage="eps"
eta::usage="eta"

Heavy::usage="Heavyside[x] with 0 at x=0"

PolesRegions::usage="See reference paper."
AnswerRegions::usage="See reference paper."
AnswerRegionsN::usage="See reference paper."

Answer::usage="See reference paper."
AnswerN::usage="See reference paper."
Poles::usage="See reference paper."

PolesQuarkRegions::usage="PolesQuarkRegions[O_0, O_1, \[CapitalPhi], rr, box, A, B, \[CurlyPhi]]"
QuarkJetRegions::usage="QuarkJetRegions[R, O, R_0, O_0, R_1, O_1, \[CapitalPhi], rr, box, A, B, s, z, \[CurlyPhi], format, file]"
QuarkJetRegionsN::usage="QuarkJetRegionsN[R, O, R_0, O_0, R_1, O_1, \[CapitalPhi], rr, box, A, B, s, z, \[CurlyPhi], method]"

PolesQuark::usage="PolesQuark[O_0, O_1, \[CapitalPhi], rr, box, A, B, \[CurlyPhi]]"
QuarkJet::usage="QuarkJet[O, O_0, O_1, \[CapitalPhi], rr, box, A, B, s, z, \[CurlyPhi], format, file]"
QuarkJetN::usage="QuarkJetN[O, O_0, O_1, \[CapitalPhi], rr, box, A, B, s, z, \[CurlyPhi], method]"

PolesGluonRegions::usage="PolesGluonRegions[O_0, O_1, \[CapitalPhi], rr, box, A, B, \[CurlyPhi], nf]"
GluonJetRegions::usage="GluonJetRegions[R, O, R_0, O_0, R_1, O_1, \[CapitalPhi], rr, box, A, B, s, z, \[CurlyPhi], nf, format, file]"
GluonJetRegionsN::usage="GluonJetRegionsN[R, O, R_0, O_0, R_1, O_1, \[CapitalPhi], rr, box, A, B, s, z, \[CurlyPhi], nf, method]"

PolesGluon::usage="PolesGluon[O_0, O_1, \[CapitalPhi], rr, box, A, B, \[CurlyPhi], nf]"
GluonJet::usage="GluonJet[O, O_0, O_1, \[CapitalPhi], rr, box, A, B, s, z, \[CurlyPhi], nf, format, file]"
GluonJetN::usage="GluonJetN[O, O_0, O_1, \[CapitalPhi], rr, box, A, B, s, z, \[CurlyPhi], nf, method]"




Begin["`Private`"]



solidangle[phi_]:=(2Sqrt[\[Pi]]  Gamma[1-eps] Sin[phi]^(-2 eps))/Gamma[1/2-eps]

splitting[k_Integer,z_]:=Module[{Pgq,Pqq,Pgg},
Pgq[zz_]:=((1+zz^2)/(1-zz)-eps(1-zz));
Pqq[zz_]:=(1-(2zz(1-zz))/(1-eps));
Pgg[zz_]:=2(zz/(1-zz)+(1-zz)/zz+zz(1-zz));
If[MemberQ[{1,2,3},k],Which[k==1,Pgq[z],k==2,Pqq[z],k==3,Pgg[z]],Print["this is not a vallid option to pick a splitting function"]]]

rapreg[yn_Symbol,z_]:=Module[{a=2},If[yn,(a)^(-eta)*(1-z)^(-eta)(z)^(-eta),1]]

Integrand[s_,z_,mu_,k_Integer,yn_Symbol]:=Module[{G,G1,G0},
G[ss_,zz_,muu_]:=Exp[eps EulerGamma](muu^2)^eps/Gamma[1-eps]*1/ss^(1+eps)(zz)^(-eps)(1-zz)^(-eps)  *splitting[k,zz]*rapreg[yn,zz];
G0[ss_,zz_,muu_]:=Normal[Series[G[ss,zz,muu],{zz,0,-1}]]; 
G1[ss_,zz_,muu_]:=Normal[Series[G[ss,zz,muu],{zz,1,-1}]]; 

{G[s,z,mu],G0[s,z,mu],G1[s,z,mu]}
]

GetConstants[k_Integer,yn_Symbol]:=Module[{a,zz,ss,muu,b1,beta1,b0,beta0,prim0,prim1,G,Gz0,Gz1},
a[s_,z_,mu_]:=Evaluate[Integrand[s,z,mu,k,yn]];
G[s_,z_,mu_]:=a[s,z,mu][[1]];
Gz0[s_,z_,mu_]:=a[s,z,mu][[2]];
Gz1[s_,z_,mu_]:=a[s,z,mu][[3]];

b0=(Exp[eps EulerGamma]*(muu^2)^eps/Gamma[1-eps]1/ss^(1+eps))^-1*Gz0[ss,zz,muu]/.zz->1;
b1=(Exp[eps EulerGamma]*(muu^2)^eps/Gamma[1-eps]1/ss^(1+eps))^-1*Gz1[ss,zz,muu]/.zz->0;

prim0[zz_]:=Series[Gz0[ss,zz,muu]*((zz)^(-eps)*Exp[eps EulerGamma]*(muu^2)^eps/Gamma[1-eps]1/ss^(1+eps))^-1,{zz,1,1}];
prim1[zz_]:=Series[Gz1[ss,zz,muu]*((1-zz)^(-eps)*Exp[eps EulerGamma]*(muu^2)^eps/Gamma[1-eps]1/ss^(1+eps))^-1,{zz,0,1}];
beta0=If[TrueQ[SeriesCoefficient[prim0[zz],0]==0],0,(b0)^(-1)*SeriesCoefficient[prim0[zz]/(1-zz),0]];
beta1=If[TrueQ[SeriesCoefficient[prim1[zz],0]==0],0,(b1)^(-1)*SeriesCoefficient[prim1[zz]/zz,0]];

{{b0,beta0},{b1,beta1}}
]




ExportModule[outputin_,outputform_,file_]:=Module[{output,instructionfile,instructionprint},
If[file==0,
	If[MemberQ[{"C","Fortran","Mathematica"},outputform],
		Which[outputform=="C",output=CForm[outputin],
			 outputform=="Fortran",output=FortranForm[outputin],
			 outputform=="Mathematica",output=outputin],
		Print["Choose a valid language: Mathematica, Fortran or C"]
	];output,
	If[MemberQ[{"C","Fortran","Mathematica"},outputform],
		Which[outputform=="C",output=ToString[CForm[outputin]];,
			 outputform=="Fortran",output=StringReplace[ToString[FortranForm[outputin]],{"ArcSin"->"ASin"}],
			outputform=="Mathematica",output=outputin
			],
	Print["Choose a valid language: Mathematica, Fortran or C"]
	];Export[file,output];


(*instructions*)
instructionfile=">> The generated file should be integrated by your own numerical integrator for all the input variables on the domain of 0 to 1.";
instructionprint=">> The generated formula should be integrated for all the input variables on the domain of 0 to 1.";If[file==0,Print[instructionprint],Print[instructionfile]];
]
]

Heavy[x_]:=If[x<=0,0,1]



theta[O_List,heavy_]:=Module[{the=0,theo=1,i,imax=Length[O],l,lmax},
For[i=1,i<= imax,i++,lmax=Length[O[[i]]];
	For[l=1,l<= lmax,l++,theo*=heavy[ O[[i,l]] ]];
	the+=theo;
	theo=1
];
the=the/.heavy[1]->1
]

thetasoft[Rsoft_List,Osoft_List,x_,xrond_,s_,heavy_]:=Module[{the=0,theo=1,i,j,imax=Length[Osoft],jmax=Length[Rsoft],xin,l,lmax,co,ao,cb,ab},

If[MemberQ[{1,0},xrond],Which[xrond==1,xin=(1-x),xrond==0,xin=x],"You can only cacluate soft terms in the 0 or 1 limit"];

For[i=1,i<=imax,i++,{{co,ao},{cb,ab}}=Osoft[[i]];
For[j=1,j<=Length[Rsoft[[i]]],j++,theo*=heavy[Rsoft[[i,j]]]];
If[cb=!=0,the+=heavy[s-co*xin^(-ao)]*heavy[cb*xin^(-ab)-s]*theo,If[co=!=0,the+=heavy[s-co*xin^(-ao)]*theo]]
];

the=the/.{heavy[s]->1,heavy[1]->1};
If[imax==jmax,the,Print["ERROR: The regions should match the observable input (lists must be of equal lengths)"]]
]

thetabar[O_List,heavy_]:=Module[{the=0,theo=1,i,imax=Length[O],l,lmax},

For[i=1,i<= imax,i++,lmax=Length[O[[i]]];
	For[l=1,l<= lmax,l++,theo*=heavy[ O[[i,l]] ]];
	the+=theo;
	theo=1
];

the=1-the;
the=the/.{heavy[1]->1}
]

thetabarsoft[Rsoft_List,Osoft_List,x_,xrond_,s_,heavy_]:=Module[{the=0,theo=1,i,j,imax=Length[Osoft],jmax=Length[Rsoft],xin,l,lmax,co,ao,cb,ab},

If[MemberQ[{1,0},xrond],Which[xrond==1,xin=(1-x),xrond==0,xin=x],Print["You can only cacluate soft terms in the 0 or 1 limit"]];

For[i=1,i<=imax,i++,{{co,ao},{cb,ab}}=Osoft[[i]];
For[j=1,j<=Length[Rsoft[[i]]],j++,theo*=heavy[Rsoft[[i,j]]]];
If[cb=!=0,the+=heavy[s-co*xin^(-ao)]*heavy[cb*xin^(-ab)-s]*theo,If[co=!=0,the+=heavy[s-co*xin^(-ao)]*theo]]
];

the=1-the;

the=the/.{heavy[s]->1,heavy[1]->1};
If[imax==jmax,the,Print["ERROR: The regions should match the observable input (lists must be of equal lengths)"]]
]

PhiConstraints[Phi_List,phi_,heavy_]:=Module[{j,theo=1,length=Length[Phi],constraint=1},
(*constraints on phi*)
If[length=!=0,
For[j=1,j<=length,j++,
	constraint=Phi[[j]]/.{phi->Pi*phi};
	 theo*=heavy[constraint];
	constraint=1;
];
];theo/.heavy[1]->1]

NumericalIntegrand[O_List,R0_List,O0_List,R1_List,O1_List,Phi_List,k_Integer,yn_Symbol,ynb_Symbol,ll_, BB_,ss_,zz_,phii_,heavy_]:=Module[{a,G,Gz0,Gz1,Gint,s,phi,under,above,jmax1=Length[R1],jmax0=Length[R0],Gintunder,Gintabove,Gintnobox,output,jac},

(*foutmeldingen uitspugen*)
If[Length[R1]!=Length[O1],Print["ERROR: The input lists for the z->1 limit should be equal length"]];
If[Length[R0]!=Length[O0],Print["ERROR: The input lists for the z->0 limit should be equal length"]
];

(*body*)
a[s_,z_,mu_]:=Evaluate[Integrand[s,z,mu,k,yn]];
G[s_,z_,mu_]:=a[s,z,mu][[1]];
Gz0[s_,z_,mu_]:=a[s,z,mu][[2]];
Gz1[s_,z_,mu_]:=a[s,z,mu][[3]];

above[l_,s_,z_,mu_]:=G[s,z,mu]*theta[O,heavy]-Gz1[s,z,mu]*thetasoft[R1,O1,z,1,s,heavy]*heavy[l-(1-z)]-Gz0[s,z,mu]*thetasoft[R0,O0,z,0,s,heavy]*heavy[l-z];

under[l_,s_,z_,mu_]:=G[s,z,mu]*thetabar[O,heavy]-Gz1[s,z,mu]*thetabarsoft[R1,O1,z,1,s,heavy]*heavy[l-(1-z)]-Gz0[s,z,mu]*thetabarsoft[R0,O0,z,0,s,heavy]*heavy[l-z];

(*Before we take the series we pick mu at the natural schale, such that all logaritms cancel. To incorporate this property we should set it to 1 before expanding. Also the input lists should have this.*)

Gintunder=heavy[BB-ss]*Normal[Series[under[ll,ss,zz,1]solidangle[phii],{eta,0,0},{eps,0,0}]];
Gintabove=heavy[ss-BB]*Normal[Series[above[ll,ss,zz,1]solidangle[phii],{eta,0,0},{eps,0,0}]];

Gintnobox=Normal[Series[above[ll,ss,zz,1]solidangle[phii],{eta,0,0},{eps,0,0}]];

(*output*)
jac=(Pi/(1-ss)^2);
If[ynb==True,
output=({Gintabove,-Gintunder}/.{ss->(ss/(1-ss)),phii->(Pi phii)})*jac,output=({Gintnobox,0}/.{ss->(ss/(1-ss)),phii->(Pi phii)})*jac
];
output=output/(2Pi)/.{heavy[zz]->1,heavy[1-zz]->1}
]

Counterterms[O0_List,O1_List,k_Integer,yn_Symbol,l_,phi_]:=Module[{b0,beta0,b1,beta1,c1b,alpha1b,c1o,alpha1o,c0o,alpha0o,c0b,alpha0b,exprb,expro,expression1=0,expression0=0,expression=0,i,j,length0=Length[O0],length1=Length[O1]},

exprb[b_,beta_,cb_,alphab_]:=(E^(eps EulerGamma)) /Gamma[1-eps]*-1/eps*(cb^-eps*b*1/(1-beta-eps(1-alphab)) (l)^(1-beta-eps(1-alphab)));

expro[b_,beta_,co_,alphao_]:=(E^(eps EulerGamma)) /Gamma[1-eps]*-1/eps*co^-eps*b*1/(1-beta-eps(1-alphao)) (l)^(1-beta-eps(1-alphao));

{{b0,beta0},{b1,beta1}}=GetConstants[k,yn];

If[Normal[Series[b1,{eta,0,0},{eps,0,0}]]!=0,
	For[i=1,i<=length1,i++,
		c1o=O1[[i,1,1]]; 
		c1b=O1[[i,2,1]]; 
		alpha1o=O1[[i,1,2]];
		alpha1b=O1[[i,2,2]] ;
		If[c1o=!=0,expression1-=expro[b1,beta1,c1o,alpha1o]];
		If[c1b=!=0,expression1+=exprb[b1,beta1,c1b,alpha1b]];
		c1o=0;c1b=0;alpha1o=0;alpha1b=0;
	];
If[expression1=!=0&&b1=!=0,Print["Counterterm for z->1 generated"]];
];

If[Normal[Series[b0,{eta,0,0},{eps,0,0}]]!=0,
	For[j=1,j<=length0,j++,
		c0o=O0[[j,1,1]];
		c0b=O0[[j,2,1]];
		alpha0o=O0[[j,1,2]];
		alpha0b=O0[[j,2,2]] ;
		If[c0o=!=0,expression0-=expro[b0,beta0,c0o,alpha0o]];
		If[c0b=!=0,expression0+=exprb[b0,beta0,c0b,alpha0b]];
		c0o=0;c0b=0;alpha0o=0;alpha0b=0;
	];
If[expression0=!=0&&b0=!=0,Print["Counterterm for z->0 generated"]];
];
expression=(expression0+expression1) 1/2 solidangle[phi]/.{phi->Pi*phi};

expression/.{0^-eps ->0,0^-eta ->0}]



Softbox[k_Integer,yn_Symbol,l_,B_]:=Module[{b0,b1,beta1,beta0,softbox},
{{b0,beta0},{b1,beta1}}=GetConstants[k,yn];

softbox=(E^(eps EulerGamma) B^-eps)/(eps*Gamma[1-eps])*(b1*1/(1-beta1-eps) l^(1-beta1-eps)+b0*1/(1-beta0-eps) l^(1-beta0-eps));
softbox]

Boxcases[k_Integer,yn_Symbol,B_]:=Module[{a=2,Boxgq,Boxqq,Boxgg,s,z,mu},
Boxgq=If[yn,-((4^(-1+eps+eta) a^-eta B^-eps E^(eps EulerGamma) (4-5 eta+eps (-5+eps+eta)) Sqrt[\[Pi]] Gamma[-eps-eta])/(eps Gamma[1-eps] Gamma[3/2-eps-eta])),(B^-eps E^(eps EulerGamma) (-4+eps) (-1+eps)^2 Gamma[-eps])/(eps Gamma[3-2 eps])];
Boxqq=If[yn,-((4^(-1+eps+eta) a^-eta B^-eps E^(eps EulerGamma) (2-eta+2 eps (-2+eps+eta)) Sqrt[\[Pi]] Gamma[1-eps-eta])/(eps Gamma[2-eps] Gamma[5/2-eps-eta])),(2 B^-eps E^(eps EulerGamma) Gamma[2-eps])/(eps (-3+2 eps) Gamma[2-2 eps])];
Boxgg=If[yn,(6 a^-eta B^-eps E^(eps EulerGamma) (-1+eps+eta)^2 (eps+eta) (-4+3 eps+3 eta) Gamma[-eps-eta]^2)/(eps^2 Gamma[-eps] Gamma[-2 (-2+eps+eta)]),(3 B^-eps E^(eps EulerGamma) (-4+3 eps) Gamma[2-eps])/(eps^2 (-3+2 eps) Gamma[2-2 eps])];
If[MemberQ[{1,2,3},k],Which[k==1,Boxgq,k==2,Boxqq,k==3,Boxgg],Print["this is not a vallid option to pick a splitting function"]]]

Boxintegral[ynb_Symbol,k_Integer,yn_Symbol,l_,B_,phi_]:=Module[{softbox,gbox},
softbox=Softbox[k,yn,l,B];
gbox=Boxcases[k,yn,B];
If[ynb,Print["Box generated"]];
If[ynb,(softbox+gbox),0]
]



PolesRegions[O0_List,O1_List,Phi_List,k_Integer,yn_Symbol,ynb_Symbol,l_, B_,phi_]:=Module[{poles,series,finite,gint2,gint3,j,length=Length[Phi],constraint=1,theo=1},
gint2=Counterterms[O0,O1,k,yn,l,phi];
gint3=Boxintegral[ynb,k,yn,l,B,phi];

series=Normal[Series[(gint2+gint3),{eta,0,0},{eps,0,0}]];
finite=series/.{1/eps->0,1/(eps^2)->0,1/(eta^2)->0,1/eta->0};
poles=series-finite//Expand;
theo=PhiConstraints[Phi,phi,HeavisideTheta];
If[FreeQ[poles,phi]&&length==0,Collect[poles//FunctionExpand//Expand,{eps,eta},Simplify],Print[StringForm[">> Answer for the poles should still be integrated over `` from 0 to 1",phi]];Collect[poles*theo//FunctionExpand//Expand,{eps,eta},Simplify]]
]


AnswerRegions[O_List,R0_List,O0_List,R1_List,O1_List,Phi_List,k_Integer,yn_Symbol,ynb_Symbol,ll_, BB_,ss_,zz_,phii_,outputform_,file_:0]:=Module[{i,numberregions=Length[O],instructionfile,instructionprint,output,integrand,gint1,gint2,gint3,series,finite,poles,theo=1},

(*ingredients*)
integrand[l_, B_,s_,z_,phi_]:=NumericalIntegrand[O,R0,O0,R1,O1,Phi,k,yn,ynb,l, B,s,z,phi,HeavisideTheta][[1]]+NumericalIntegrand[O,R0,O0,R1,O1,Phi,k,yn,ynb,l, B,s,z,phi,HeavisideTheta][[2]];

gint2=Counterterms[O0,O1,k,yn,ll,phii];
gint3=Boxintegral[ynb,k,yn,ll,BB,phii];
series=Normal[Series[((gint2+gint3)),{eta,0,0},{eps,0,0}]]//FunctionExpand//Expand;
finite=series/.{1/eps->0,1/eta->0,1/eps^2->0,1/eta^2->0};

(*constraints on phi*)
theo=PhiConstraints[Phi,phii,HeavisideTheta];

(*output*)
output=integrand[ll,BB,ss,zz,phii]+finite//Expand;
output=output*theo;
output=Collect[output,{HeavisideTheta[a_]},Factor];

ExportModule[output,outputform,file]
]


AnswerRegionsN[O_List,R0_List,O0_List,R1_List,O1_List,Phi_List,k_Integer,yn_Symbol,ynb_Symbol,ll_, BB_,ss_,zz_,phii_,method_:"GaussKronrodRule"]:=Module[{i,numberregions=Length[O],theo=1,length=Length[Phi],integrand,gint1,gint2,gint3,series,finite,poles,answer=0},


(*ingredients*)
integrand[l_, B_,s_,z_,phi_]:=NumericalIntegrand[O,R0,O0,R1,O1,Phi,k,yn,ynb,l, B,s,z,phi,Heavy];

gint2=Counterterms[O0,O1,k,yn,ll,phii];
gint3=Boxintegral[ynb,k,yn,ll,BB,phii];
series=Normal[Series[((gint2+gint3)),{eta,0,0},{eps,0,0}]]/.{PolyGamma[0,1/2]->-2Log[2]-EulerGamma,1/eps->0,1/eta->0,1/(eps^2)->0,1/(eta^2)->0}//Expand;

(*constraints on phi*)
theo=PhiConstraints[Phi,phii,Heavy];

(*body*)
If[FreeQ[series,phii]&&length==0,finite=series,finite=NIntegrate[series*theo,{phii,0,1}]];

If[integrand[ll,BB,ss,zz,phii][[2]]===0,
If[length==0&&FreeQ[Total[integrand[ll,BB,ss,zz,phii]],phii],
	gint1=NIntegrate[Total[integrand[ll,BB,ss,zz,phii]],{zz,0,1},{ss,0,1}, Method->method];
	answer=gint1+finite,
	gint1=NIntegrate[Total[integrand[ll,BB,ss,zz,phii]]*theo,{phii,0,1},{zz,0,1},{ss,0,1},Method->method];
	answer=gint1+finite
],
If[length==0&&FreeQ[Total[integrand[ll,BB,ss,zz,phii]],phii],
gint1=NIntegrate[integrand[ll,BB,ss,zz,phii][[1]],{zz,0,1}, {ss,0,1},Method->method]+NIntegrate[integrand[ll,BB,ss,zz,phii][[2]],{zz,0,1},{ss,0,1}, Method->method];
	answer=gint1+finite ,
gint1=NIntegrate[integrand[ll,BB,ss,zz,phii][[1]]*theo,{phii,0,1},{zz,0,1}, {ss,0,1},Method->method]+NIntegrate[integrand[BB,ll,ss,zz,phii][[2]]*theo,{phii,0,1},{zz,0,1},{ss,0,1}, Method->method];
	answer=gint1+finite
]
];

(*output*)
answer //N
]

Answer[O_,O0_,O1_,Phi_,k_Integer,yn_Symbol,ynb_Symbol,ll_, BB_,ss_,zz_,phii_,outputform_,file_:0]:=Module[{list={1}},
AnswerRegions[{O},list,{O0},list,{O1},Phi,k,yn,ynb,ll,BB,ss,zz,phii,outputform,file]
]

AnswerN[O_,O0_,O1_,Phi_,k_Integer,yn_Symbol,ynb_Symbol,ll_, BB_,ss_,zz_,phii_,method_:"GaussKronrodRule"]:=Module[{list={1}},
AnswerRegionsN[{O},list,{O0},list,{O1},Phi,k,yn,ynb,ll,BB,ss,zz,phii,method]
]

Poles[O0_List,O1_List,Phi_List,k_Integer,yn_Symbol,ynb_Symbol,l_, B_,phi_]:=Module[{},
PolesRegions[{O0},{O1},Phi,k,yn,ynb,l, B,phi]
]

PolesQuarkRegions[O0_List,O1_List,Phi_List,yn_Symbol,ynb_Symbol,l_,B_,phi_]:=Module[{TF=1/2,CA=3,CF=4/3},
CF*PolesRegions[O0,O1,Phi,1,yn,ynb,l, B,phi]//Expand
]

QuarkJetRegions[O_List,R0_List,O0_List,R1_List,O1_List,Phi_List,yn_Symbol,ynb_Symbol,ll_,BB_,ss_,zz_,phii_,outputform_,file_:0]:=Module[{ans,TF=1/2,CA=3,CF=4/3},
ans=AnswerRegions[O,R0,O0,R1,O1,Phi,1,yn,ynb,ll,BB,ss,zz,phii,"Mathematica"];  
ExportModule[ans*CF,outputform,file]
]

QuarkJetRegionsN[O_List,R0_List,O0_List,R1_List,O1_List,Phi_List,yn_Symbol,ynb_Symbol,ll_,BB_,ss_,zz_,phii_,method_:"GaussKronrodRule"]:=Module[{ans,TF=1/2,CA=3,CF=4/3},
ans=AnswerRegionsN[O,R0,O0,R1,O1,Phi,1,yn,ynb,ll,BB,ss,zz,phii,method];ans*CF
]

PolesQuark[O0_List,O1_List,Phi_List,yn_Symbol,ynb_Symbol,l_,B_,phi_]:=Module[{TF=1/2,CA=3,CF=4/3},Collect[CF*Poles[O0,O1,Phi,1,yn,ynb,l, B,phi]//Expand,{eps,eta}]
]

QuarkJet[O_,O0_,O1_,Phi_,yn_Symbol,ynb_Symbol,ll_,BB_,ss_,zz_,phii_,outputform_,file_:0]:=Module[{ans,list={1},TF=1/2,CA=3,CF=4/3},
QuarkJetRegions[{O},list,{O0},list,{O1},Phi,yn,ynb,ll,BB,ss,zz,phii,outputform,file]
]

QuarkJetN[O_,O0_,O1_,Phi_,yn_Symbol,ynb_Symbol,ll_, BB_,ss_,zz_,phii_,method_:"GaussKronrodRule"]:=Module[{ans,list={1},TF=1/2,CA=3,CF=4/3},
QuarkJetRegionsN[{O},list,{O0},list,{O1},Phi,yn,ynb,ll,BB,ss,zz,phii,method]
]


PolesGluonRegions[O0_List,O1_List,Phi_List,yn_Symbol,ynb_Symbol,l_, B_,phi_,nf_]:=Module[{TF=1/2,CA=3,CF=4/3},
Collect[(nf*TF*PolesRegions[O0,O1,Phi,2,yn,ynb,l, B,phi]+1/2*CA*PolesRegions[O0,O1,Phi,3,yn,ynb,l, B,phi])//Expand,{eps,eta}]
]

GluonJetRegions[O_List,R0_List,O0_List,R1_List,O1_List,Phi_List,yn_Symbol,ynb_Symbol,ll_, BB_,ss_,zz_,phii_,outputform_,nf_,file_:0]:=Module[{i,numberregions=Length[O],TF=1/2,CA=3,CF=4/3,instructionfile,instructionprint,output,integrand,gint1,gint2,gint3,series,finite,poles,k,l,safe,theo},

(*ingredients*)
integrand[l_, B_,s_,z_,phi_,kk_]:=NumericalIntegrand[O,R0,O0,R1,O1,Phi,kk,yn,ynb,l, B,s,z,phi,HeavisideTheta][[1]]+NumericalIntegrand[O,R0,O0,R1,O1,Phi,kk,yn,ynb,l, B,s,z,phi,HeavisideTheta][[2]];

(*constraints on phi*)
theo=PhiConstraints[Phi,phii,HeavisideTheta];

(*body*)
For[k=2,k<=3,k++,
gint2=Counterterms[O0,O1,k,yn,ll,phii];
gint3=Boxintegral[ynb,k,yn,ll,BB,phii];
series=Normal[Series[((gint2+gint3)),{eta,0,0},{eps,0,0}]]/.{PolyGamma[0,1/2]->-2Log[2]-EulerGamma};
finite=N[series]/.{1/eps->0,1/eta->0,1/eps^2->0,1/eta^2->0};

safe[k]=integrand[ll,BB,ss,zz,phii,k]+finite/.{0.->0}//Expand;

gint2=0;
gint3=0;
series=0;
finite=0;
];

output=nf*TF*safe[2]+1/2*CA*safe[3];

(*output*)
output=output*theo;
output=Collect[output,{HeavisideTheta[a_]},Factor];
ExportModule[output,outputform,file]
]

GluonJetRegionsN[O_List,R0_List,O0_List,R1_List,O1_List,Phi_List,yn_Symbol,ynb_Symbol,ll_, BB_,ss_,zz_,phii_,nf_,method_:"GaussKronrodRule"]:=Module[{list={1},TF=1/2,CA=3,CF=4/3},
nf*TF*AnswerRegionsN[O,R0,O0,R1,O1,Phi,2,yn,ynb,ll,BB,ss,zz,phii,method]+1/2*CA*AnswerRegionsN[O,R0,O0,R1,O1,Phi,3,yn,ynb,ll,BB,ss,zz,phii,method]
]

PolesGluon[O0_List,O1_List,Phi_List,yn_Symbol,ynb_Symbol,l_, B_,phi_,nf_]:=Module[{TF=1/2,CA=3,CF=4/3},
Collect[(nf*TF*PolesRegions[{O0},{O1},Phi,2,yn,ynb,l, B,phi]+1/2*CA*PolesRegions[{O0},{O1},Phi,3,yn,ynb,l, B,phi])//Expand,{eps,eta},Simplify]
]

GluonJet[O_,O0_,O1_,Phi_,yn_Symbol,ynb_Symbol,ll_, BB_,ss_,zz_,phii_,outputform_,nf_,file_:0]:=Module[{list={1}},GluonJetRegions[{O},list,{O0},list,{O1},Phi,yn,ynb,ll,BB,ss,zz,phii,outputform,nf,file]
]

GluonJetN[O_,O0_,O1_,Phi_,yn_Symbol,ynb_Symbol,ll_, BB_,ss_,zz_,phii_,nf_,method_:"GaussKronrodRule"]:=Module[{list={1},TF=1/2,CA=3,CF=4/3},
GluonJetRegionsN[{O},list,{O0},list,{O1},Phi,yn,ynb,ll,BB,ss,zz,phii,nf,method]
]
















End[]

EndPackage[]
